<?php

namespace Drupal\numerology_calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a "numerology calculator ajax submit form block".
 *
 * @Block(
 *   id = "numerology_calculator_block",
 *   admin_label = @Translation("Numerology Calculator block")
 * )
 */
class NumerologyAjaxFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\numerology_calculator\Form\NameCalculator');
    return $form;
  }

}
