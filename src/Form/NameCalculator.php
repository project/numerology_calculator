<?php

namespace Drupal\numerology_calculator\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Implementing a numerology calculator form.
 */
class NameCalculator extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'name_calculator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_tcmessage"></div>',
    ];

    $form['tck_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    ];

    $form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::setNameCalcMessage',
      ],
    ];

    return $form;
  }

  /**
   * Setting the message in our form.
   */
  public function setNameCalcMessage(array $form, FormStateInterface $form_state) {
	$output = '';
    // If name is not empty.
    if (!empty($form_state->getValue('tck_username'))) {
      $tck_username = $form_state->getValue('tck_username');
      // Including helper functions inc file.
      module_load_include('inc', 'numerology_calculator', 'numerology_calculator.helper_functions');
      // Getting output.
      $output = numerology_calculator_get_results($tck_username);
    }
    else {
      $output = 'ERROR: Provide vaild Name';
    }
    // debug($form_state->getValue('tck_username'), $label = 'date', $print_r = TRUE);
    $response = new AjaxResponse();
    $response->addCommand(
      new HtmlCommand(
        '.result_tcmessage',
        '<div class="my_top_message">' . t('Your numerology number is @result', ['@result' => $output]) . '</div>'),
    );
    return $response;
  }

  /**
   * Submitting the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
