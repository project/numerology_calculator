<?php

/**
 * @file
 * Defines helper functions.
 */

/**
 * Helper function to calculated numerology for name.
 *
 * @param object $num_username
 *   The name string.
 *
 * @return number
 *   The calculated number.
 */
function numerology_calculator_get_results($num_username) {
  $output = "";
  $val_ar = array(
	'A' => 1,
	'B' => 2,
	'C' => 3,
	'D' => 4,
	'E' => 5,
	'F' => 8,
	'G' => 3,
	'H' => 5,
	'I' => 1,
	'J' => 1,
	'K' => 2,
	'L' => 3,
	'M' => 4,
	'N' => 5,
	'O' => 7,
	'P' => 8,
	'Q' => 1,
	'R' => 2,
	'S' => 3,
	'T' => 4,
	'U' => 6,
	'V' => 6,
	'W' => 6,
	'X' => 5,
	'Y' => 1,
	'Z' => 7);
	//get the value entered by submission method
	$str = $num_username;
	//make it upper case. to avoid messing with small letters.
	$str = strtoupper($str);
	//find the length of the string entered
	$len = strlen($str);
	 
	//set a temp value to calculate
	$numero_val = 0;
	 
	//now loop through the string one by one and add the values
	for($i=0; $i<$len; $i++)
	{
	$alpha_val  = $str[$i];
	$numero_val = $val_ar[$alpha_val] + $numero_val;
	}
  
  // Returning calculated age.
  return $numero_val;
}

